package max.smarthome.smarthomesevice.controller

import max.smarthome.smarthomesevice.common.HomeId
import max.smarthome.smarthomesevice.model.Room
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/api/house")
class RoomController {
    private val rooms = listOf(
            Room(
                    UUID.fromString("5b078107-49fd-4c5c-9549-38566f90cfd7"),
                    "Bathroom",
                    UUID.fromString("80f736a1-3dfc-4d8e-abf3-fc3d610dd4cb")
            )
    )

    @GetMapping("/{homeId}/rooms")
    fun rooms(@PathVariable homeId: HomeId): List<Room> {
        return rooms
                .filter {
                    it.houseId == homeId
                }
    }
}
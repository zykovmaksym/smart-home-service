package max.smarthome.smarthomesevice.controller

import max.smarthome.smarthomesevice.common.UserId
import max.smarthome.smarthomesevice.model.Home
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/user")
class HomeController {
    private var homeList = listOf(
            Home(
                   UUID.fromString("80f736a1-3dfc-4d8e-abf3-fc3d610dd4cb"),
                   "My home",
                   listOf(
                           UUID.fromString("d9ef7352-8cf4-4fb8-ae4b-735926e8ef33")
                   )
            )
    )

    @GetMapping("/{userId}/houses")
    fun homes(@PathVariable userId: UserId): List<Home> {
        return homeList
                .filter {
                    it.users.contains(userId)
                }
    }

    @PostMapping("/{userId}/house")
    fun addHome(@PathVariable userId: UserId, @RequestBody home: Home) {
        homeList = homeList.plus(home)
    }

    @DeleteMapping("/{userId}/house")
    fun removeHome(@PathVariable userId: UserId, @RequestBody home: Home) {
        homeList = homeList.minus(home)
    }
}
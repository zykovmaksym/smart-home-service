package max.smarthome.smarthomesevice.controller

import max.smarthome.smarthomesevice.api.RegisterDeviceRequest
import max.smarthome.smarthomesevice.api.RegisterDeviceResponse
import max.smarthome.smarthomesevice.common.RoomId
import max.smarthome.smarthomesevice.devicestorageapi.DeviceStorageApi
import max.smarthome.smarthomesevice.model.Device
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api")
class DeviceController(private val deviceStorageApi: DeviceStorageApi) {
    var devices = listOf<Device>(
//            Device(
//                    UUID.fromString("d9ef7352-8cf4-4fb8-ae4b-735926e8ef34"),
//                    "sensor1",
//                    "RelayDevice",
//                    UUID.fromString("5b078107-49fd-4c5c-9549-38566f90cfd7")
//            )
    )

    @GetMapping("/room/{roomId}/devices")
    fun devices(@PathVariable roomId: RoomId): List<Device> {
        return devices
                .filter {
                    it.roomId == roomId
                }
    }

    @PostMapping("/device/register")
    fun registerDevice(@RequestBody registerDeviceRequest: RegisterDeviceRequest): ResponseEntity<RegisterDeviceResponse> {
        println("request: $registerDeviceRequest")
        val deviceStorage = deviceStorageApi.fetchDeviceByMac(registerDeviceRequest.mac).execute().body()!!
        val device = Device(deviceStorage.id, registerDeviceRequest.title, deviceStorage.type, registerDeviceRequest.roomId)
        devices = devices + device
        return ResponseEntity.ok(RegisterDeviceResponse(deviceStorage.id))
    }
}
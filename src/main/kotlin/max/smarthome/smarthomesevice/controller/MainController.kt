package max.smarthome.smarthomesevice.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class MainController {
    @GetMapping("/public")
    fun public(): String {
        return "This is public"
    }

    @GetMapping("/private")
    fun private(): String {
        return "This is private"
    }
}
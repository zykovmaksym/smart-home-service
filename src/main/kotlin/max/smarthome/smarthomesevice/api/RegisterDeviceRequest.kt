package max.smarthome.smarthomesevice.api

import max.smarthome.smarthomesevice.common.RoomId

data class RegisterDeviceRequest(
        val mac: String,
        val title: String,
        val roomId: RoomId
)

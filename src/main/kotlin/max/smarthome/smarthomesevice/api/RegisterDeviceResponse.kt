package max.smarthome.smarthomesevice.api

import max.smarthome.smarthomesevice.common.DeviceId

data class RegisterDeviceResponse(
        val deviceId: DeviceId
)

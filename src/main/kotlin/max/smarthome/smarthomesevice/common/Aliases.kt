package max.smarthome.smarthomesevice.common

import java.util.*

typealias HomeId = UUID
typealias UserId = UUID
typealias RoomId = UUID
typealias DeviceId = UUID
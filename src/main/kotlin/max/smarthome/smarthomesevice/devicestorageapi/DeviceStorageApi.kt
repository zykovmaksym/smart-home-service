package max.smarthome.smarthomesevice.devicestorageapi

import max.smarthome.smarthomesevice.devicestorageapi.api.DeviceStorageResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface DeviceStorageApi {
    @GET("/api/device/{mac}")
    fun fetchDeviceByMac(@Path("mac") mac: String): Call<DeviceStorageResponse>
}
package max.smarthome.smarthomesevice.devicestorageapi.api

import java.util.*

data class DeviceStorageResponse(
        val id: UUID,
        val type: String
)

package max.smarthome.smarthomesevice.devicestorageapi

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Configuration
class DeviceStorageApiConfig {
    @Bean
    fun provideDeviceStorageApi(): DeviceStorageApi {
        val retrofit = Retrofit.Builder()
                .baseUrl("http://192.168.0.132:8100")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        return retrofit.create(DeviceStorageApi::class.java)
    }
}
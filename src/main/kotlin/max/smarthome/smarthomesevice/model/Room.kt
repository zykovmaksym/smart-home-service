package max.smarthome.smarthomesevice.model

import max.smarthome.smarthomesevice.common.HomeId
import max.smarthome.smarthomesevice.common.RoomId

data class Room(
        val id: RoomId,
        val title: String,
        val houseId: HomeId
)

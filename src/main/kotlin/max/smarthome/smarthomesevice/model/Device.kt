package max.smarthome.smarthomesevice.model

import max.smarthome.smarthomesevice.common.DeviceId
import max.smarthome.smarthomesevice.common.RoomId

data class Device(
        val deviceId: DeviceId,
        val title: String,
        val type: String,
        val roomId: RoomId
)

package max.smarthome.smarthomesevice.model

import max.smarthome.smarthomesevice.common.HomeId
import max.smarthome.smarthomesevice.common.UserId

data class Home(
        val id: HomeId,
        val title: String,
        val users: List<UserId>
)
